import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'

import { IScheme } from 'src/app/model/scheme'
import { UtilService } from './util.service'

@Injectable({ providedIn: 'root' })
export class SchemeService {
  constructor(private utilService: UtilService) {}

  private _schemeList: IScheme[] = [
    {
      name: 'name',
      label: 'name',
      input_type: 'input',
      options: {
        required: true,
        type: 'input',
      },
      validators: [{ type: 'required' }],
    },
    {
      name: 'checkone',
      label: 'checkone',
      input_type: 'checkbox',
      validators: [{ type: 'requiredTrue' }],
    },
    {
      name: 'checktwo',
      label: 'checktwo',
      input_type: 'checkbox',
      // TODO:: Make it work
      // groupOptions: [
      //   {
      //     name: 'check2',
      //     input_type: 'checkbox',
      //     label: 'sur',
      //     id: 'sur',
      //     value: 'sur',
      //   },
      //   {
      //     name: 'check2',
      //     input_type: 'checkbox',
      //     label: 'zxc',
      //     id: 'zxc',
      //     value: 'zxc',
      //   },
      //   {
      //     name: 'check2',
      //     input_type: 'checkbox',
      //     label: 'asd',
      //     id: 'asd',
      //     value: 'asd',
      //   },
      // ],
      validators: [{ type: 'required' }],
    },
    {
      name: 'dar',
      label: 'dar',
      input_type: 'radio',
      value: 'rqw',
      validators: [{ type: 'required' }],
    },
    {
      name: 'dar2',
      input_type: 'radio',
      groupOptions: [
        {
          name: 'dar2',
          input_type: 'radio',
          label: 'sur',
          id: 'sur',
          value: 'sur',
        },
        {
          name: 'dar2',
          input_type: 'radio',
          label: 'zxc',
          id: 'zxc',
          value: 'zxc',
        },
        {
          name: 'dar2',
          input_type: 'radio',
          label: 'asd',
          id: 'asd',
          value: 'asd',
        },
      ],
      validators: [{ type: 'required' }],
    },
    {
      name: 'surname',
      label: 'surname',
      input_type: 'input',
      options: {
        autocomplete: 'surname',
      },
      validators: [{ type: 'required' }],
    },
    {
      name: 'age',
      label: 'age',
      input_type: 'input',
      validators: [{ type: 'required' }],
    },
    {
      name: 'new-password',
      label: 'password',
      input_type: 'input',
      options: {
        autocomplete: 'new-password',
      },
      validators: [
        { type: 'required' },
        { type: 'pattern', data: /^[A-Za-zd0-9]{8,16}$/ },
        { type: 'triggerValidation', data: 'new-password_confirm' },
      ],
    },
    {
      name: 'new-password_confirm',
      label: 'password_confirm',
      input_type: 'input',
      options: {
        autocomplete: 'new-password',
        type: 'password',
      },
      validators: [
        { type: 'required' },
        { type: 'minLength', data: 8 },
        { type: 'maxLength', data: 16 },
        { type: 'isPasswordSame', data: 'new-password' },
      ],
    },
    {
      name: 'country',
      label: 'country',
      input_type: 'select',
      selectOptions: [
        { label: 'Seçiniz', value: null },
        { label: '2', value: 2 },
      ],
      validators: [{ type: 'required' }],
    },
  ]

  public get schemeList() {
    return new BehaviorSubject(this._schemeList.sort((a, b) => this.utilService.compareFn(a, b, 'order')))
  }
}
