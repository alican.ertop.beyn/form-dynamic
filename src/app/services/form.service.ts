import { Injectable } from '@angular/core'
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms'

import { IScheme } from 'src/app/model'

type IOpts = {
  onlySelf?: boolean | undefined
  emitEvent?: boolean | undefined
}

type IControl = { [key: string]: AbstractControl }
type IValidatorsFn = (v: any) => ValidationErrors | ValidatorFn

const labelKeyValue: Record<string, string> = {
  name: 'Isim',
  surname: 'Soyisim',
  age: 'Yaş',
  password: 'Şifre',
  password_confirm: 'Şifre Kontrol',
  country: 'Şehir',
}

const errorKeyMessage: Record<string, (v: any, key: string) => string> = {
  required: () => 'Zorunlu alan',
  isPasswordSame: (v) => 'Şifre Aynı değil',
  isSame: (v) => (typeof v === 'string' ? v : 'Aynı değil'),
  maxlength: (v: { requiredLength: number; actualLength: number }) => `Maximum ${v.requiredLength} karakter olabilir`,
  minlength: (v: { requiredLength: number; actualLength: number }) => `Minimum ${v.requiredLength} karakter gereklidir`,
  pattern: (v: { requiredPattern: string; actualLength: number }, key) => `Minimum 8 karakter,Maksimum 16 karakter`,
}

@Injectable()
export class FormService {
  formGroup: FormGroup = new FormGroup({})
  private schemeList?: IScheme[]

  constructor(private fb: FormBuilder) {}

  private generateFormGroupObject = (scheme: IScheme[]) => {
    return scheme.reduce((prev, { initialValue, name, validators }) => {
      prev[name] = new FormControl(
        initialValue ?? prev?.[name]?.value,
        validators
          ?.map((validator) => this.validatorsKeyFn?.[validator?.type]?.(validator.data))
          ?.filter(Boolean) as ValidatorFn[],
      )

      return prev
    }, {} as Record<string, FormControl>)
  }

  private isPasswordSame = (givenKey: string) => (ctrl: FormControl) =>
    ctrl.parent?.get?.(givenKey)?.value !== ctrl?.value ? { isPasswordSame: true } : null

  private isSame = (givenArr: string[], errMessage?: string) => (ctrl: FormControl) =>
    ctrl.parent?.get?.(givenArr)?.value !== ctrl?.value ? { isSame: errMessage ? errMessage : true } : null

  private triggerValidation = (givenKey: string, opts?: IOpts) => (ctrl: FormControl) =>
    ctrl.parent?.get?.(givenKey)?.updateValueAndValidity?.(opts)

  labelFromKey = (key: string) => {
    const label = labelKeyValue[key] ?? key
    const scheme = this.schemeList?.find((scheme) => scheme.label === key)
    if (scheme && scheme?.validators?.some((validator) => validator.type === 'required')) return `*${label}`

    return label
  }

  validatorsKeyFn: Record<string, IValidatorsFn> = {
    pattern: Validators.pattern,
    minLength: Validators.minLength,
    maxLength: Validators.maxLength,
    required: () => Validators.required,
    isPasswordSame: this.isPasswordSame.bind(this),
    triggerValidation: this.triggerValidation.bind(this),
    requiredTrue: () => Validators.requiredTrue,
    isSame: this.isSame.bind(this),
  }

  errorFromKey = (key: string) => {
    if (!this.formGroup.controls[key].touched || !this.formGroup || !key) return []
    const errors = this.formGroup.get(key)?.errors
    return errors ? Object.keys(errors).map((key) => errorKeyMessage[key]?.(errors?.[key], key)) : []
  }

  setSchemeList(scheme: IScheme[]) {
    this.schemeList = scheme
  }

  generateGroup(scheme: IScheme[]) {
    this.setSchemeList(scheme)
    this.formGroup = this.fb.group(this.generateFormGroupObject(scheme))
    return this.formGroup
  }

  submit(formGroup?: FormGroup, params?: { resetable: boolean }): { [x: string]: any } | undefined {
    const _formGroup = this.formGroup ?? formGroup
    if (_formGroup.valid) {
      const values = { ..._formGroup.value }
      if (params?.resetable) _formGroup.reset()
      return values
    } else {
      _formGroup.markAllAsTouched()
      _formGroup.updateValueAndValidity({ emitEvent: true })
      return undefined
    }
  }
}
