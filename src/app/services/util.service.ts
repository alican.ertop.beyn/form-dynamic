import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UtilService {
  compareFn = (a: any, b: any, key: string) => {
    if (a[key] < b[key]) return -1;
    if (a[key] > b[key]) return 1;
    return 0;
  };
}
