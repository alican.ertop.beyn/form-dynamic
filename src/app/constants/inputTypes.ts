import { IInputType } from 'src/app/model'

import { InputComponent } from 'src/app/components/form/input/input.component'
import { SelectComponent } from 'src/app/components/form/select/select.component'
import { RadioComponent } from '../components/form/radio/radio.component'
import { CheckboxComponent } from '../components/form/checkbox/checkbox.component'

export const inputTypeToComponent: Record<IInputType, typeof InputComponent> = {
  input: InputComponent,
  select: SelectComponent,
  radio: RadioComponent,
  checkbox: CheckboxComponent,
}

export const inputTypeToNativeInputType: Record<IInputType, any> = {
  input: 'text',
  radio: 'radio',
  select: 'select',
  checkbox: 'checkbox',
}
