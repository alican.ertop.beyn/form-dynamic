import { Component, OnInit } from '@angular/core'

import { FormService } from 'src/app/services'

@Component({
  selector: 'app-form-label',
  templateUrl: './form-label.component.html',
  styleUrls: ['./form-label.component.scss'],
})
export class FormLabelComponent implements OnInit {
  public name: string = 'undefined'
  public labelKey: string = ''

  constructor(private formService: FormService) {}

  get labelFromKey() {
    return () => this.formService.labelFromKey(this.labelKey)
  }

  ngOnInit(): void {}
}
