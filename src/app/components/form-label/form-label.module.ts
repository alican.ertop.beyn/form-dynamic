import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormLabelComponent } from './form-label.component';

@NgModule({
  imports: [BrowserModule],
  declarations: [FormLabelComponent],
  entryComponents: [FormLabelComponent],
  exports: [FormLabelComponent],
})
export class FormLabelModule {}
