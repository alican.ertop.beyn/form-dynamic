import { Component, OnInit } from '@angular/core'

import { FormService } from 'src/app/services'

@Component({
  selector: 'app-form-error',
  templateUrl: './form-error.component.html',
  styleUrls: ['./form-error.component.scss'],
})
export class FormErrorComponent implements OnInit {
  public key: string = ''

  constructor(private formService: FormService) {}

  public get errorByKey() {
    return (key: string) => this.formService.errorFromKey(key)
  }

  ngOnInit(): void {}
}
