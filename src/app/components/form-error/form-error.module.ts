import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormErrorComponent } from './form-error.component';

@NgModule({
  imports: [BrowserModule],
  declarations: [FormErrorComponent],
  entryComponents: [FormErrorComponent],
  exports: [FormErrorComponent],
})
export class FormErrorModule {}
