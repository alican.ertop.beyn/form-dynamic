import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { Subject, takeUntil } from 'rxjs'
import { IInputBase } from 'src/app/bases'

import { IScheme } from 'src/app/model'
import { SchemeService, FormService } from 'src/app/services'
import { FormResolver } from '../../resolver/form.resolver'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit, OnDestroy {
  @Input() public component?: IInputBase

  public formGroup: FormGroup = new FormGroup({})
  public schemeList: IScheme[] = []

  private $destroy = new Subject()

  constructor(private schemeService: SchemeService, private formService: FormService, public resolver: FormResolver) {}

  ngOnInit(): void {
    this.schemeService.schemeList.pipe(takeUntil(this.$destroy)).subscribe((scheme) => {
      this.formGroup = this.formService.generateGroup(scheme)

      this.schemeList = scheme
    })

    // this.formGroup.valueChanges.pipe(takeUntil(this.$destroy)).subscribe((o) => {
    //   // console.log(o, this.formGroup.controls);
    // })
  }

  ngOnDestroy(): void {
    this.$destroy.complete()
  }

  onSubmit() {
    const values = this.formService.submit()
    console.log(values)
  }
}
