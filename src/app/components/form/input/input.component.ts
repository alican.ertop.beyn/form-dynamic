import { Component, OnInit } from '@angular/core'
import { IInputBase } from 'src/app/bases/InputBase'

@Component({
  selector: '.app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent extends IInputBase implements OnInit {
  constructor() {
    super()
  }

  ngOnInit(): void {}
}
