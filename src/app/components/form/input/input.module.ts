import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { InputComponent } from './input.component'

import { DirectiveModule } from 'src/app/directives'

import { FormResolver } from 'src/app/resolver'

@NgModule({
  imports: [BrowserModule, ReactiveFormsModule, FormsModule, DirectiveModule],
  providers: [FormResolver.provider('input', InputComponent)],
  declarations: [InputComponent],
})
export class InputModule {}
