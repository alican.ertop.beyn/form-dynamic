import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { SelectComponent } from './select.component'
import { DirectiveModule } from 'src/app/directives'

import { FormResolver } from 'src/app/resolver'

@NgModule({
  imports: [BrowserModule, DirectiveModule, ReactiveFormsModule, FormsModule],
  providers: [FormResolver.provider('select', SelectComponent)],
  declarations: [SelectComponent],
})
export class SelectModule {}
