import { Component, OnInit } from '@angular/core'
import { IInputBase } from 'src/app/bases/InputBase'

@Component({
  selector: '.app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
})
export class SelectComponent extends IInputBase implements OnInit {
  constructor() {
    super()
    this.input_type = 'select'
  }

  ngOnInit(): void {}
}
