import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { CheckboxComponent } from './checkbox.component'

import { DirectiveModule } from 'src/app/directives'
import { FormResolver } from 'src/app/resolver'

@NgModule({
  imports: [BrowserModule, ReactiveFormsModule, FormsModule, DirectiveModule],
  providers: [FormResolver.provider('checkbox', CheckboxComponent)],
  declarations: [CheckboxComponent],
})
export class CheckboxModule {}
