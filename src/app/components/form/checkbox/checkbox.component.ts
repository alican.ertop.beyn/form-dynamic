import { Component, OnInit } from '@angular/core'
import { IInputBase } from 'src/app/bases'

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
})
export class CheckboxComponent extends IInputBase implements OnInit {
  constructor() {
    super()
    this.input_type = 'checkbox'
  }

  ngOnInit(): void {}
}
