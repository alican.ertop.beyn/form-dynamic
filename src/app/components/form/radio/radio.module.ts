import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'


import { RadioComponent } from './radio.component'

import { DirectiveModule } from 'src/app/directives'
import { FormResolver } from 'src/app/resolver'

@NgModule({
  imports: [BrowserModule, ReactiveFormsModule, FormsModule, DirectiveModule],
  providers: [FormResolver.provider('radio', RadioComponent)],
  declarations: [RadioComponent],
})
export class RadioModule {}
