import { Component, OnInit } from '@angular/core'
import { IInputBase } from 'src/app/bases'

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
})
export class RadioComponent extends IInputBase implements OnInit {
  constructor() {
    super()
    this.input_type = 'radio'
  }

  ngOnInit(): void {}
}
