import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { NgxdModule } from '@ngxd/core'

import { FormErrorModule } from 'src/app/components/form-error'
import { DirectiveModule } from 'src/app/directives'

import { RadioModule } from './radio/radio.module'
import { SelectModule } from './select/select.module'
import { CheckboxModule } from './checkbox/checkbox.module'
import { InputModule } from './input/input.module'

import { FormComponent } from './form.component'
import { FormResolver } from 'src/app/resolver/'
import { FormService } from 'src/app/services'

@NgModule({
  imports: [
    NgxdModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FormErrorModule,
    InputModule,
    CheckboxModule,
    RadioModule,
    SelectModule,
    DirectiveModule,
  ],
  providers: [FormService, FormResolver],
  declarations: [FormComponent],
  entryComponents: [FormComponent],
  exports: [FormComponent],
})
export class FormModule {}
