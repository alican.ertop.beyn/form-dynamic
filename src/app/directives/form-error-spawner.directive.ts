import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core'

import { FormErrorComponent } from 'src/app/components/form-error'

@Directive({ selector: '[formErrorSpawner]' })
export class FormErrorSpawnerDirective {
  constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {}

  @Input() set formErrorSpawner(key: string) {
    if (!key) {
      this.viewContainerRef.clear()
      return
    }

    this.templateRef.createEmbeddedView(this.templateRef)
    const { instance } = this.viewContainerRef.createComponent(FormErrorComponent)
    instance.key = key
  }
}
