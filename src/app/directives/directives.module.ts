import { NgModule } from '@angular/core'

import {
  FormErrorSpawnerDirective,
  FormLabelSpawnerDirective,
  InputAttrSetterDirective,
  InputSpawnerDirective,
} from './'

export const DECLARATIONS = [
  FormErrorSpawnerDirective,
  FormLabelSpawnerDirective,
  InputAttrSetterDirective,
  InputSpawnerDirective,
]

@NgModule({
  imports: [],
  declarations: DECLARATIONS,
  exports: DECLARATIONS,
})
export class DirectiveModule {}
