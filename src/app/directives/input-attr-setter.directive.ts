import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core'

import { IScheme } from 'src/app/model'
import { getNativeInputType } from 'src/app/utils'

@Directive({ selector: '[inputAttrSetter]' })
export class InputAttrSetterDirective implements OnInit {
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  @Input('inputAttrSetter') scheme?: IScheme

  ngOnInit(): void {
    this.optionsMapping()
    this.setNameValueIdAttr()
    this.styleMapping()
    this.classMapping()
    this.inputTypeMapping()
    this.checkRequiredValidator()
  }

  private isEmpty = (c: any) => typeof c === 'undefined' || c === null || c.length === 0

  setNameValueIdAttr() {
    if (Boolean(this.scheme?.id)) this.setAttr('id', this.scheme?.id)
    else this.setAttr('id', this.scheme?.name)

    this.setAttr('name', this.scheme?.name)
    this.setAttr('value', this.scheme?.value)
  }

  inputTypeMapping() {
    this.setAttr('type', getNativeInputType(this.scheme?.input_type))
  }

  optionsMapping() {
    const options: Record<string, any> = this.scheme?.options ?? {}
    Object.keys(options).forEach((key) => this.setAttr(key, options?.[key]))
  }

  styleMapping() {
    Object.keys(this.scheme?.style ?? {}).forEach((key) => {
      this.setStyle(key, this.scheme?.style?.[key])
    })
  }

  classMapping() {
    this.scheme?.class?.forEach((c) => this.setClass(c))
  }

  checkRequiredValidator() {
    if ((this.scheme?.validators ?? []).some((f) => f.type === 'required')) this.setAttr('required', true)
  }

  setStyle(name: string, value: any) {
    if (this.isEmpty(value)) return
    this.renderer.setStyle(this.elementRef.nativeElement, name, value)
  }

  setClass(value: any) {
    if (this.isEmpty(value)) return
    this.renderer.addClass(this.elementRef.nativeElement, value)
  }

  setAttr(name: string, value: any, namespace?: string) {
    if (this.isEmpty(value)) return

    this.renderer.setAttribute(this.elementRef.nativeElement, name, value, namespace)
  }
}
