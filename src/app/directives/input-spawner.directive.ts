import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core'
import { FormGroup } from '@angular/forms'

import { IScheme } from 'src/app/model'
import { getComponentFromType } from 'src/app/utils'

@Directive({ selector: '[inputSpawner]' })
export class InputSpawnerDirective {
  constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {}

  @Input() set inputSpawner(params: [IScheme, FormGroup]) {
    if (params.some((b) => !b)) {
      this.viewContainerRef.clear()
      return
    }
    const [scheme, formGroup] = params ?? []
    this.templateRef.createEmbeddedView(this.templateRef)
    const { instance } = this.viewContainerRef.createComponent(getComponentFromType(scheme.input_type))

    instance.scheme = scheme
    instance.formGroup = formGroup
  }
}
