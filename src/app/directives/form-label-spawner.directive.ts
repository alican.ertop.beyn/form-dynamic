import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core'

import { FormLabelComponent } from 'src/app/components/form-label'

type IName = string
type IKey = string

@Directive({ selector: '[formLabelSpawner]' })
export class FormLabelSpawnerDirective {
  constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {}

  @Input() set formLabelSpawner(params: [IName, IKey?]) {
    if (params.some((b) => !b)) {
      this.viewContainerRef.clear()
      return
    }
    const [name, key] = params ?? []

    this.templateRef.createEmbeddedView(this.templateRef)
    const { instance } = this.viewContainerRef.createComponent(FormLabelComponent)

    instance.name = name
    instance.labelKey = key ?? 'undefined'
  }
}
