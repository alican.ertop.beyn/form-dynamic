import { IInputType } from 'src/app/model'
import { inputTypeToComponent } from 'src/app/constants'

export const getComponentFromType = (inputType: IInputType = 'input') => {
  return inputTypeToComponent[inputType]
}
