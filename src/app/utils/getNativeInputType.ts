import { inputTypeToNativeInputType } from 'src/app/constants';
import { IInputType } from 'src/app/model';

export const getNativeInputType = (inputType?: IInputType) =>
  inputType ? inputTypeToNativeInputType[inputType] : 'text';
