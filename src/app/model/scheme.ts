import { IInputType, ISelectOption, IValidator } from '.'

export interface IScheme {
  id?: string
  name: string
  input_type: IInputType
  label?: string
  order?: number
  initialValue?: string | number | boolean
  value?: string | number | boolean
  disabled?: boolean
  class?: string[]
  style?: { [x: string]: string }
  selectOptions?: ISelectOption[]
  groupOptions?: IScheme[]
  options?: Partial<HTMLInputElement>
  validators?: IValidator[]
}
