export interface IValidator {
  type: string
  error?: string
  data?: number | string | boolean | RegExp
}
