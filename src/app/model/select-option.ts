export interface ISelectOption {
  value?: string | number | null
  label: string
}
