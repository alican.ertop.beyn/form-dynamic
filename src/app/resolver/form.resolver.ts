import { Inject, Injectable, InjectionToken, Provider, Type } from '@angular/core'

import { IInputType } from 'src/app/model'
import { IInputBase } from '../bases'

export interface IFormProvider {
  type: IInputType
  component: Type<any>
}

@Injectable()
export class FormResolver {
  static PROVIDE_TOKEN = new InjectionToken<IFormProvider[]>('Form Provider')

  static provider(type: IInputType, component: Type<IInputBase>): Provider {
    return {
      provide: FormResolver.PROVIDE_TOKEN,
      useValue: { type, component },
      multi: true,
    }
  }

  private config: Map<IInputType, any>

  constructor(@Inject(FormResolver.PROVIDE_TOKEN) providers: IFormProvider[]) {
    this.config = providers.reduce((config, provider) => config.set(provider.type, provider.component), new Map())
  }

  resolve = (type: IInputType): Type<any> => this.config.get(type)
}
