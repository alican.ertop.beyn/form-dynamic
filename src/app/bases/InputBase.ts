import { Component, Input } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { IScheme, IInputType } from 'src/app/model'

@Component({ template: '' })
export class IInputBase {
  input_type: IInputType = 'input'
  @Input('formGroup') formGroup: FormGroup = new FormGroup({})
  @Input('scheme') scheme?: IScheme

  constructor() {}

  get groupOptions() {
    return (this.scheme?.groupOptions?.length ? this.scheme.groupOptions : [this.scheme]) as IScheme[]
  }
}
